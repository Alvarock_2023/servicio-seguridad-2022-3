pipeline {

    agent any

    tools {
        maven 'mvn 3.8.1'
    }

    environment {
        SONAR_SCANNER = tool 'SonarScanner'
        GCP_REPOSITORY = "us-central1-docker.pkg.dev/curso-devops-2022-caltamirano/develop"
        GCP_REPOSITORY_PROD = "us-central1-docker.pkg.dev/curso-devops-2022-caltamirano/produccion"
        GCP_REGION = "https://us-central1-docker.pkg.dev"
        KEY_SERVICE_ACCOUNT = credentials('key-service-account')
    }

    stages {

        stage('Compilar') {
            steps {
                echo 'Compilando...'
                sh 'mvn clean compile'
            }
        }

        stage('Pruebas') {
            steps {
                echo 'Ejecutando pruebas...'
                sh 'mvn test -Dspring.profiles.active=test'
            }
            post {
                success {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }

        stage('Análisis de Código EStático') {
            steps {
                echo 'Analizando código...'

                withSonarQubeEnv('sonarqube-server') {
                    sh "${SONAR_SCANNER}/bin/sonar-scanner -Dproject.settings=jenkins/sonar.properties"
                }

                timeout(time: 3, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }

            }
        }

        stage('Build') {
            steps {
                echo 'Generando imagen docker...'
                sh "mvn package -DskipTests"
                sh "docker build -t $GCP_REPOSITORY/servicio-seguridad:$BUILD_NUMBER ."
            }
        }

        stage('Publicar Build') {
            steps {
                echo 'Publicar imagen en google artifact registry...'
                sh "docker login -u _json_key --password-stdin $GCP_REGION < $KEY_SERVICE_ACCOUNT"
                sh "docker push $GCP_REPOSITORY/servicio-seguridad:$BUILD_NUMBER"
            }
            post {
                always {
                    echo "Eliminar imagenes generadas..."
                    sh "docker rmi $GCP_REPOSITORY/servicio-seguridad:$BUILD_NUMBER"
                }
            }
        }

        stage('Desplegar') {
            steps {
                echo "Desplegando servicio en cloud run..."
                sh "gcloud auth activate-service-account curso-devops@curso-devops-2022-caltamirano.iam.gserviceaccount.com --key-file=$KEY_SERVICE_ACCOUNT"
                sh "gcloud run deploy servicio-seguridad --image=$GCP_REPOSITORY/servicio-seguridad:$BUILD_NUMBER --port=8080 --region=us-central1 --project=curso-devops-2022-caltamirano --allow-unauthenticated"
            }
        }


    }

}